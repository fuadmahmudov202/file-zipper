package az.bakudynamics.filezipper.repository;

import az.bakudynamics.filezipper.entity.File;
import az.bakudynamics.filezipper.enums.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FileRepo extends CrudRepository<File,Long> {

    Optional<File> findById(Long id);

    List<File> findByStatus(Status status);
}
