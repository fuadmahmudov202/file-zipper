package az.bakudynamics.filezipper.repository;

import az.bakudynamics.filezipper.entity.Users;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepo extends CrudRepository<Users,Long> {

    Optional<Users> findByEmail(String email);
}
