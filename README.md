#File Zipper Application

It's an application which u can zip your folder and files. You should just run application and write uri of your folder or file application will create zip file and inform user. Also you can check status of your proses. 
I used H2 database in this project u can access database with this [url]{http://localhost:8080/h2-console} <br>
For using swagger you can use this [link]{http://localhost:8080/swagger-ui.html}
for authentication please use [username]{fuadmahmudov202@gmail.com} [password]{1234} <br>
<br>
You should enter path with 2 slash on swagger if you add this link:<br>
{<br>
"path": "D:\video1"<br>
}<br>
it will give result to us :<br>
{<br>
"id": 1,<br>
"filePath": "D:\video1",<br>
"zipPath": null,<br>
"status": "IN_PROGRESS",<br>
"type": "FOLDER"<br>
}<br>
after completion zipping proses zip path will add also and status change to COMPLATED<br>
<br>
You can check your tasks with id in this [url]{http://localhost:8080/status/1}
<br>
this url will give us task which has id=1<br>
{<br>
"id": 1,<br>
"filePath": "D:\video1",<br>
"zipPath": "D:\video1.zip",<br>
"status": "COMPLETED",<br>
"type": "FOLDER"<br>
}<br>