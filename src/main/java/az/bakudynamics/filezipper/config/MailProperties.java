package az.bakudynamics.filezipper.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application.config.mail")
@Data
public class MailProperties {
    private String subject;
    private String content;
}
