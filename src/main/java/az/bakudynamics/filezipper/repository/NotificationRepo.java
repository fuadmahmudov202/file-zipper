package az.bakudynamics.filezipper.repository;

import az.bakudynamics.filezipper.entity.Notification;
import az.bakudynamics.filezipper.enums.NotificationStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NotificationRepo extends CrudRepository<Notification, Long> {
    List<Notification> findByIsSent(NotificationStatus status);
}
