package az.bakudynamics.filezipper.mapper;

import az.bakudynamics.filezipper.dto.FileDto;
import az.bakudynamics.filezipper.entity.File;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE , componentModel = "spring")
public interface FileMapper {

    FileDto mapToDto(File file);
}
