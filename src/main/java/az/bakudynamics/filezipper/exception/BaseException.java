
package az.bakudynamics.filezipper.exception;

import az.bakudynamics.filezipper.enums.ResultCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BaseException extends RuntimeException{
    private final ResultCode result;
}
