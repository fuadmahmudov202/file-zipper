package az.bakudynamics.filezipper.exception;

import az.bakudynamics.filezipper.enums.ResultCode;

public class FileException extends BaseException{

    public FileException(ResultCode resultCode) {
        super(resultCode);
    }

    public static FileException fileNotFound(){
        return new FileException(ResultCode.FILE_NOT_FOUND);
    }

    public static FileException fileTypeUndefined(){
        return new FileException(ResultCode.FILE_TYPE_UNDEFINED);
    }
}
