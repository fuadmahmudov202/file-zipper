package az.bakudynamics.filezipper.utils;

import az.bakudynamics.filezipper.constants.Constants;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FileZipper {

    public static Optional<Path> zipFolder(String uri) {
        Path sourcePath = Paths.get(uri);
        Path zipPath = Paths.get(uri.concat(Constants.ZIP_EXTENSION));

        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipPath.toFile()))) {

            Files.walkFileTree(sourcePath, new SimpleFileVisitor<>() {
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    zos.putNextEntry(new ZipEntry(sourcePath.relativize(file).toString()));
                    Files.copy(file, zos);
                    zos.closeEntry();
                    return FileVisitResult.CONTINUE;
                }
            });

            return Optional.of(zipPath);

        } catch (Exception e) {
            log.error("failed to zip folder , path: {}", sourcePath.getFileName() );
            return Optional.empty();
        }

    }



    public static Optional<Path> zipSingleFile(String uri) {
        Path source = Paths.get(uri);
        String zipFileName = source.toString().concat(Constants.ZIP_EXTENSION);
        try (ZipOutputStream zos = new ZipOutputStream(
                        new FileOutputStream(zipFileName));
                FileInputStream fis = new FileInputStream(source.toFile()) ){

            ZipEntry zipEntry = new ZipEntry(source.getFileName().toString());
            zos.putNextEntry(zipEntry);

            byte[] buffer = new byte[1024];
            int len;
            while ((len = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            zos.closeEntry();
            return Optional.of(source);
        }catch (Exception e){
            log.error("failed to zip regular file path: {}: ", source.getFileName());
            return Optional.empty();
        }

    }
}
