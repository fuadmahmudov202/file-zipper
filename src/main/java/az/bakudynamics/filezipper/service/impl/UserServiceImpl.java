package az.bakudynamics.filezipper.service.impl;

import az.bakudynamics.filezipper.repository.UserRepo;
import az.bakudynamics.filezipper.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;

    public static String currentUser() {
        var context= SecurityContextHolder.getContext();
        return SecurityContextHolder.getContext()
                .getAuthentication()
                .getName();
    }
}
