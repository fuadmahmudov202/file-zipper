package az.bakudynamics.filezipper.tasks;

import az.bakudynamics.filezipper.config.MailProperties;
import az.bakudynamics.filezipper.entity.File;
import az.bakudynamics.filezipper.entity.Notification;
import az.bakudynamics.filezipper.enums.NotificationStatus;
import az.bakudynamics.filezipper.repository.NotificationRepo;
import az.bakudynamics.filezipper.utils.MailSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
@Slf4j
@Component
public class NotificationTasks {

    private final NotificationRepo notificationRepo;
    private final MailProperties mailProperties;


    @Scheduled(fixedDelay = 10000)
    @Transactional
    public void zipFiles() {
        log.info("...  schedule  started to scan unsent notification ...");

        List<Notification> notifications = notificationRepo.findByIsSent(NotificationStatus.UNSENT);

        notifications.forEach((this::sendMail));

        log.info("...  schedule  finished to scan notification ...");
    }


    public void sendMail(Notification notification) {
        var file = notification.getFile();
        if (Objects.isNull(file))
            return;

        MailSender.sendMail(file.getInsertBy(),
                mailProperties.getSubject(),
                getMailContent(file));
        notification.setIsSent(NotificationStatus.SENT);
        notificationRepo.save(notification);
    }

    private String getMailContent(File file) {

        if (Objects.isNull(file))
            return "";

      return   MessageFormat.format(mailProperties.getContent(),
                file.getId(),
                file.getFilePath(),
                file.getZipPath());
    }
}