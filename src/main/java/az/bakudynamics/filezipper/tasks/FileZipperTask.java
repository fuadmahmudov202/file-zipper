package az.bakudynamics.filezipper.tasks;

import az.bakudynamics.filezipper.entity.File;
import az.bakudynamics.filezipper.enums.Status;
import az.bakudynamics.filezipper.repository.FileRepo;
import az.bakudynamics.filezipper.service.NotificationService;
import az.bakudynamics.filezipper.utils.FileZipper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class FileZipperTask {

    private final FileRepo fileRepo;
    private final NotificationService notificationService;


    @Scheduled(fixedDelay = 5000)
    public void zipFiles() {
        log.info("...  schedule  started to scan files ...");

        List<File> files = fileRepo.findByStatus(Status.IN_PROGRESS);

        files.forEach((this::zipFile));

        log.info("...  schedule  finished to scan files ...");

    }


    @Transactional
    public void zipFile(File file) {
        switch (file.getType()) {

            case FILE:
                zipSingleFile(file);
                break;

            case FOLDER:
                zipFolder(file);
                break;

            default:
                return;
        }
        fileRepo.save(file);
        notificationService.save(file);

    }

    public void zipSingleFile(File file) {
        var zipPath = FileZipper.zipSingleFile(file.getFilePath());
        if (zipPath.isPresent()) {
            file.setStatus(Status.COMPLETED);
            file.setZipPath(zipPath.get().toString());
        } else
            file.setStatus(Status.FAILED);

    }

    public void zipFolder(File file) {
        var zipPath = FileZipper.zipFolder(file.getFilePath());
        if (zipPath.isPresent()) {
            file.setStatus(Status.COMPLETED);
            file.setZipPath(zipPath.get().toString());
        } else
            file.setStatus(Status.FAILED);
    }

}
