package az.bakudynamics.filezipper.model;

import az.bakudynamics.filezipper.constants.AppConstants;
import az.bakudynamics.filezipper.enums.ResultCode;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ApiResult {
    private final int code;
    private final String message;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConstants.DATE_TIME_FORMAT)
    private LocalDateTime timestamp;

    public ApiResult(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getValue();
        this.timestamp = LocalDateTime.now();
    }
    public ApiResult(int code, String message) {
        this.code = code;
        this.message = message;
        this.timestamp = LocalDateTime.now();
    }
}
