package az.bakudynamics.filezipper.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum FileType {
    FILE(1),
    FOLDER(2);

    private final int value;

    private static final Map<Integer,FileType> VALUE_MAP= new HashMap<>();

    static {
        for (FileType type:values()) {
            VALUE_MAP.put(type.value,type);
        }
    }

    public static FileType getStatus(Integer status){
        return VALUE_MAP.get(status);
    }

}
