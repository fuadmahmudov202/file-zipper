package az.bakudynamics.filezipper.entity;

import az.bakudynamics.filezipper.entity.converter.NotificationStatusConverter;
import az.bakudynamics.filezipper.enums.NotificationStatus;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "NOTIFICATION")
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILE")
    private File file;

    @Column(name = "IS_SENT")
    @Convert(converter = NotificationStatusConverter.class)
    private NotificationStatus isSent;

}
