package az.bakudynamics.filezipper.controller;

import az.bakudynamics.filezipper.dto.FileDto;
import az.bakudynamics.filezipper.request.FileReq;
import az.bakudynamics.filezipper.service.FileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Slf4j
@RestController
@RequiredArgsConstructor
public class MainController {

    private final FileService fileService;

    @PostMapping("/zip")
    public FileDto zipFolder(@Valid @RequestBody FileReq req){
        log.info("endpoint called: /zip , method: POST");
        return fileService.save(req);
    }

    @GetMapping("/status/{id}")
    public FileDto getStatus(@PathVariable Long id) {
        log.info("endpoint called: /status/{} , method: GET",id);
       return fileService.findById(id);
    }


}
