package az.bakudynamics.filezipper.config;



import az.bakudynamics.filezipper.exception.LoginException;
import az.bakudynamics.filezipper.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Primary
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) {

        var userLogin=userRepo.findByEmail(username)
                .orElseThrow(LoginException::userNotFound);
        return new UserDetailsImpl(userLogin);
    }
}
