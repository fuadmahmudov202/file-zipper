package az.bakudynamics.filezipper.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {

    public static final String ZIP_EXTENSION= ".zip";
}
