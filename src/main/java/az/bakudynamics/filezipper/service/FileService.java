package az.bakudynamics.filezipper.service;

import az.bakudynamics.filezipper.dto.FileDto;
import az.bakudynamics.filezipper.request.FileReq;


public interface FileService {

    FileDto findById(Long id);

    FileDto save(FileReq req);
}
