package az.bakudynamics.filezipper.service.impl;

import az.bakudynamics.filezipper.entity.File;
import az.bakudynamics.filezipper.entity.Notification;
import az.bakudynamics.filezipper.enums.NotificationStatus;
import az.bakudynamics.filezipper.repository.NotificationRepo;
import az.bakudynamics.filezipper.service.NotificationService;
import az.bakudynamics.filezipper.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {

    private final NotificationRepo notificationRepo;
    private final UserService userService;

    @Override
    public Notification save(File file) {
        Notification notification = new Notification();
        notification.setFile(file);
        notification.setIsSent(NotificationStatus.UNSENT);
        return notificationRepo.save(notification);
    }

}
