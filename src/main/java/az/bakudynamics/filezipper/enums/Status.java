package az.bakudynamics.filezipper.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum Status {
    FAILED(0),
    IN_PROGRESS(1),
    COMPLETED(2);

    private final int value;

    private static final Map<Integer,Status> VALUE_MAP= new HashMap<>();

    static {
        for (Status status:values()) {
            VALUE_MAP.put(status.value,status);
        }
    }

    public static Status getStatus(Integer status){
        return VALUE_MAP.get(status);
    }
}
