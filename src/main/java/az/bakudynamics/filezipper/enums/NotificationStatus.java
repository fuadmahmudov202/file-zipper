package az.bakudynamics.filezipper.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum NotificationStatus {
    UNSENT(0),
    SENT(1);

    private final int value;

    private static final Map<Integer,NotificationStatus> VALUE_MAP= new HashMap<>();

    static {
        for (NotificationStatus status:values()) {
            VALUE_MAP.put(status.value,status);
        }
    }

    public static NotificationStatus getStatus(Integer status){
        return VALUE_MAP.get(status);
    }
}
