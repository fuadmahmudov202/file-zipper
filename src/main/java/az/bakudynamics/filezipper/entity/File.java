package az.bakudynamics.filezipper.entity;

import az.bakudynamics.filezipper.entity.converter.FileTypeConverter;
import az.bakudynamics.filezipper.entity.converter.StatusConverter;
import az.bakudynamics.filezipper.enums.FileType;
import az.bakudynamics.filezipper.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "FILE")
public class File {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID",insertable = false)
    private Long id;

    @Column(name = "FILE_PATH")
    private String filePath;

    @Column(name = "ZIP_PATH")
    private String zipPath;

    /*1-FILE , 2-FOLDER */
    @Column(name = "TYPE")
    @Convert(converter = FileTypeConverter.class)
    private FileType type;

    /*1-IN_PROGRESS , 0-FAILED, 2-COMPLETED*/
    @Column(name = "STATUS")
    @Convert(converter = StatusConverter.class)
    private Status status;

    @Column(name = "INSERT_BY")
    private String insertBy;



}
