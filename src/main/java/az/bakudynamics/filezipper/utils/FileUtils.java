package az.bakudynamics.filezipper.utils;

import az.bakudynamics.filezipper.enums.FileType;
import az.bakudynamics.filezipper.exception.FileException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.nio.file.Files;
import java.nio.file.Path;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileUtils {

    public static FileType getFileType(Path path) {
        if (Files.isRegularFile(path))
            return FileType.FILE;

        else if (Files.isDirectory(path))
            return FileType.FOLDER;

        else
            throw FileException.fileTypeUndefined();
    }
}
