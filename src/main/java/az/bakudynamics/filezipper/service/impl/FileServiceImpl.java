package az.bakudynamics.filezipper.service.impl;

import az.bakudynamics.filezipper.dto.FileDto;
import az.bakudynamics.filezipper.entity.File;
import az.bakudynamics.filezipper.enums.Status;
import az.bakudynamics.filezipper.exception.FileException;
import az.bakudynamics.filezipper.mapper.FileMapper;
import az.bakudynamics.filezipper.repository.FileRepo;
import az.bakudynamics.filezipper.request.FileReq;
import az.bakudynamics.filezipper.service.FileService;
import az.bakudynamics.filezipper.utils.FileUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

import static az.bakudynamics.filezipper.service.impl.UserServiceImpl.currentUser;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final FileRepo fileRepo;
    private final FileMapper fileMapper;

    @Override
    public FileDto save(FileReq req) {
        var path =Paths.get(req.getPath());

        if (!Files.exists(path))
            throw FileException.fileNotFound();

        var fileType = FileUtils.getFileType(path);

        var file =File.builder()
                .filePath(req.getPath())
                .type(fileType)
                .status(Status.IN_PROGRESS)
                .insertBy(currentUser())
                .build();

        fileRepo.save(file);

        return fileMapper.mapToDto(file);
    }

    @Override
    public FileDto findById(Long id) {
        Optional<File> file =fileRepo.findById(id);

        if (file.isEmpty())
            throw FileException.fileNotFound();

        return fileMapper.mapToDto(file.get());
    }


}
