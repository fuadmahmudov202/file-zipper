package az.bakudynamics.filezipper.utils;

import lombok.extern.slf4j.Slf4j;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

@Slf4j
public final class MailSender {

    private MailSender() {
    }

    public static void sendMail(String toMail, String subject, String content) {
        var props = getSmtpProperties();
        if (props.isEmpty())
            return;
        var properties = props.get();
        try {
            Session session = Session.getInstance(properties, new MailAuth(
                    properties.getProperty("mail.username"),
                    properties.getProperty("mail.pass")));
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress (properties.getProperty("mail.username")));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toMail));
            message.setSubject(subject);
            message.setText(content);

            Transport.send(message);
            log.info("Message has been sent successfully");

        } catch (MessagingException e) {
            log.warn("email failed to send:",e);


        }

    }

    private static Optional<Properties> getSmtpProperties() {
        Properties prop = new Properties();
        try (InputStream fs = MailSender.class.getClassLoader().getResourceAsStream("smtp.properties")) {

            prop.load(fs);
            return Optional.of(prop);
        } catch (Exception e) {
            log.error("unable to read smtp properties");
            return Optional.empty();
        }

    }

}

class MailAuth extends Authenticator {

    private String username;
    private String password;

    public MailAuth(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);

    }

}

