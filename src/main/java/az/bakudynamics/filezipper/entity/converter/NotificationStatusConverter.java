package az.bakudynamics.filezipper.entity.converter;

import az.bakudynamics.filezipper.enums.NotificationStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class NotificationStatusConverter implements AttributeConverter< NotificationStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn( NotificationStatus attribute) {
        return attribute.getValue();
    }

    @Override
    public NotificationStatus convertToEntityAttribute(Integer dbData) {
        return  NotificationStatus.getStatus(dbData);
    }
}
