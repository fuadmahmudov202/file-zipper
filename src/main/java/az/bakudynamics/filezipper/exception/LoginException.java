package az.bakudynamics.filezipper.exception;

import az.bakudynamics.filezipper.enums.ResultCode;

public class LoginException extends BaseException{

    public LoginException(ResultCode resultCode) {
        super(resultCode);
    }

    public static LoginException userNotFound() {
        return new LoginException(ResultCode.USER_NOT_FOUND);
    }
}
