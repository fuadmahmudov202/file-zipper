package az.bakudynamics.filezipper.entity.converter;

import az.bakudynamics.filezipper.enums.FileType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class FileTypeConverter implements AttributeConverter<FileType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(FileType attribute) {
        return attribute.getValue();
    }

    @Override
    public FileType convertToEntityAttribute(Integer dbData) {
        return FileType.getStatus(dbData);
    }
}
