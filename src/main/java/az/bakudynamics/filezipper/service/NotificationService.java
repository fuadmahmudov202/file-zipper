package az.bakudynamics.filezipper.service;

import az.bakudynamics.filezipper.entity.File;
import az.bakudynamics.filezipper.entity.Notification;

public interface NotificationService {
    Notification save(File file);
}
