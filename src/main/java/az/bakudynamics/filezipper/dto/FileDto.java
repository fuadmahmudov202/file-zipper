package az.bakudynamics.filezipper.dto;

import az.bakudynamics.filezipper.enums.FileType;
import az.bakudynamics.filezipper.enums.Status;
import lombok.Data;

@Data
public class FileDto {
    private Long id;
    private String filePath;
    private String zipPath;
    private Status status;
    private FileType type;

}
